<?php 

namespace Vidasoft\Backtracing\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BacktracingController extends Controller
{
    public function greet($greet = "Hi World!")
    {
        return view('backtracing::backtracing', compact('greet'));
    }
}
