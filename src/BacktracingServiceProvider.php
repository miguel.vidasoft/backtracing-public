<?php

namespace Vidasoft\Backtracing;

use Illuminate\Support\ServiceProvider;

class BacktracingServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // register our controller
        $this->app->make('Vidasoft\Backtracing\Controllers\BacktracingController');
        $this->loadViewsFrom(__DIR__.'/views', 'backtracing');
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        
        //include __DIR__.'/routes.php';
        // $configPath = __DIR__ . '/../config/backtracing.php';
        // $this->publishes([$configPath => $this->getConfigPath()], 'config');

        $this->loadRoutesFrom(realpath(__DIR__ . '/routes/web.php'));

        // $this->registerMiddleware(InjectDebugbar::class);

        // if ($this->app->runningInConsole()) {
        //     $this->commands(['command.backtracing.clear']);
        // }
    }
}